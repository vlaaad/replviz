(ns matter.impl
  (:require [clojure.core.cache :as core.cache]
            [clojure.set :as set]))

(defn- assert-not-leaked [cache sub-id]
  (assert (not (core.cache/has? cache sub-id))
          (str (first sub-id) " is attempting to subscribe to bound matter which already has value

Possible reasons:
- you return lazy seq which uses `matter.api/sub`
- you leaked matter from matter-fn's scope without unbinding it first (call `matter.api/unbind` on it in that case)")))

(defn unbind [matter]
  (dissoc matter :*deps :parent-sub-id))

(defn- calc-sub-value [matter sub-id]
  (let [[k & args] sub-id
        *deps (atom {})
        sub-fn (get-in matter [:subs k] k)
        bound-matter (assoc matter :*deps *deps :parent-sub-id sub-id)]
    {:value (apply sub-fn bound-matter args)
     :depends-on @*deps}))

(defn sub [matter k & args]
  (let [sub-id (apply vector k args)
        {:keys [*cache *deps]} matter
        cache @*cache
        ret (-> (cond
                  (core.cache/has? cache sub-id)
                  (do (swap! *cache core.cache/hit sub-id)
                      cache)

                  (core.cache/has? cache [::dirty sub-id])
                  (let [dirty-sub (core.cache/lookup cache [::dirty sub-id])
                        deps (:depends-on dirty-sub)
                        unbound-matter (unbind matter)]
                    (if (= deps
                           (->> deps
                                keys
                                (map (juxt identity
                                           #(apply sub unbound-matter %)))
                                (into {})))
                      (swap! *cache (fn [cache]
                                      (-> cache
                                          (core.cache/evict [::dirty sub-id])
                                          (core.cache/miss sub-id dirty-sub))))
                      (let [v (calc-sub-value matter sub-id)]
                        (swap! *cache (fn [cache]
                                        (-> cache
                                            (core.cache/evict [::dirty sub-id])
                                            (core.cache/miss sub-id v)))))))

                  :else
                  (swap! *cache core.cache/miss sub-id (calc-sub-value matter sub-id)))
                (core.cache/lookup sub-id)
                :value)]
    (when *deps
      (assert-not-leaked cache (:parent-sub-id matter))
      (swap! *deps (fn [deps]
                     (if (= :matter deps)
                       :matter
                       (assoc deps sub-id ret)))))
    ret))

(defn- make-reverse-deps [cache]
  (reduce (fn [acc [sub-id {:keys [depends-on]}]]
            (if (= :matter depends-on)
              (update acc :matter (fnil conj #{}) sub-id)
              (reduce (fn [acc dep-sub-id]
                        (update acc dep-sub-id (fnil conj #{}) sub-id))
                      acc
                      (keys depends-on))))
          {}
          cache))

(defn- gather-dirty-deps-impl [acc sub-ids reverse-deps]
  (reduce (fn [acc sub-id]
            (if (contains? acc sub-id)
              acc
              (-> acc
                  (conj sub-id)
                  (gather-dirty-deps-impl (reverse-deps sub-id) reverse-deps))))
          acc
          sub-ids))

(defn- gather-dirty-deps [sub-ids reverse-deps]
  (reduce (fn [acc sub-id]
            (gather-dirty-deps-impl acc (reverse-deps sub-id) reverse-deps))
          #{}
          sub-ids))

(defn invalidate-cache [cache old-subs new-subs]
  (let [changed-keys (->> old-subs
                          keys
                          (remove #(= (old-subs %) (new-subs %)))
                          set)
        changed-sub-ids (->> cache
                             keys
                             (filter #(contains? changed-keys (first %)))
                             set)
        reverse-deps (make-reverse-deps cache)
        sub-ids-to-remove (set/union changed-sub-ids (reverse-deps :matter #{}))
        dirty-sub-ids (gather-dirty-deps sub-ids-to-remove reverse-deps)
        cache-with-removed-sub-ids (reduce core.cache/evict cache sub-ids-to-remove)]
    (reduce (fn [acc sub-id]
              (-> acc
                  (core.cache/evict sub-id)
                  (core.cache/miss [::dirty sub-id] (core.cache/lookup acc sub-id))))
            cache-with-removed-sub-ids
            dirty-sub-ids)))

(defn advance-subs [matter f & args]
  (let [{:keys [subs *cache *deps]} matter
        cache @*cache
        new-subs (apply f subs args)]
    (when *deps
      (assert-not-leaked cache (:parent-sub-id matter))
      (reset! *deps :matter))
    {:subs new-subs
     :*cache (atom (invalidate-cache cache subs new-subs))}))

(defn make [subs cache-factory]
  {:subs subs
   :*cache (atom (cache-factory {}))})
