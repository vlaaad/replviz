(ns matter.api
  (:require [clojure.core.cache :as core.cache]
            [matter.impl :as impl]))

(def ^:private kw->cache-factory
  {:basic core.cache/basic-cache-factory
   :fifo core.cache/fifo-cache-factory
   :lru core.cache/lru-cache-factory
   :ttl core.cache/ttl-cache-factory
   :lu core.cache/lu-cache-factory
   :lirs core.cache/lirs-cache-factory
   :soft core.cache/soft-cache-factory})

(defn make
  "Create matter with supplied `subs` and `cache-factory` (defaults to :basic)

  `subs` is a map from subscription keys to matter-fns (fns that receive matter as first
  argument)
  `cache-factory` may be a custom cache factory function or one of `:basic`, `:fifo`,
  `:lru`, `:ttl`, `:lu`, `:lirs`, `:soft` for corresponding default cache factories with
  default settings"
  ([subs]
   (make subs :basic))
  ([subs cache-factory]
   (impl/make subs (kw->cache-factory cache-factory cache-factory))))

(defn sub [matter k & args]
  (apply impl/sub matter k args))

(defn advance-subs [matter f & args]
  (apply impl/advance-subs matter f args))

(defn set-value [matter k v]
  (advance-subs matter assoc k (constantly v)))

(defn advance-value [matter k f & args]
  (let [new-value (apply f (sub matter k) args)]
    (set-value matter k new-value)))

(defn unbind [matter]
  (impl/unbind matter))
