(ns replviz
  (:require [cljfx.api :as cljfx]
            [matter.api :as matter]
            [matter-cljfx.core :as matter-cljfx])
  (:import [javafx.scene.input KeyCode]))

(set! *warn-on-reflection* true)

;; escape hatch to force-re-render everything?

;; TODO:
;; - get rid of *opts*, pass them instead
;; - :on-text-changed handler dispatches during advancing, maybe it shouldn't?
;; - make jfx-media and jfx-web optional
;; - pane props should be set through meta?
;; - some props do not create instances, they use provided instead (dialog pane in dialog)
;; - default focus traversable of controls!
;; - default style classes!
;; - default on-x-changed prop change listeners!
;; - default managed properties in controls

;; - update to same desc should be identical (component-vec)
;; - should I use more namespaced keywords?

;; basic example
(cljfx/on-fx-thread
  (cljfx/create-component
    [:stage
     {:showing true
      :always-on-top true
      :style :transparent}
     [:scene
      {:fill :transparent
       :stylesheets #{"styles.css"}}
      [:v-box
       [:text-area {:wrap-text true :text "blop" :pref-row-count 2}]
       [:label {:effect [:effect/bloom
                         [:effect/drop-shadow
                          {:radius 1
                           :offset-y 2}]]
                :tooltip [:tooltip "hello!"]
                :context-menu [:context-menu
                               [:menu.item/default "blep"]
                               [:menu.item/radio {:selected true} "blop"]]}
        "Hi! What's your name?"]
       [:text-field]
       [:separator]
       [:password-field]
       [:tool-bar
        [:button {:text "boop"}]
        [:button {:text "boop"}]
        [:separator]
        [:button {:text "boop"}]]]]]))

(defn function-example []
  (let [text-input (fn [label-text]
                     [:v-box
                      [:label label-text]
                      [:text-field]])]
    (cljfx/on-fx-thread
      (cljfx/create-component
        [:stage
         {:always-on-top true
          :style :transparent
          :showing true}
         [:scene
          {:fill :transparent
           :on-key-pressed prn
           :stylesheets #{"styles.css"}}
          [:v-box
           [text-input "First Name"]
           [text-input "Last Name"]]]]))))

#_(function-example)

;; map event handler
(defn map-event-handler []
  (let [*state (atom [:stage
                      {:always-on-top true
                       :style :transparent
                       :showing true}
                      [:scene
                       {:fill :transparent
                        :on-key-pressed {:event :event/scene-key-press}
                        :stylesheets #{"styles.css"}}
                       [:v-box
                        [:label "Hi! What's your name?"]
                        [:text-field]]]])
        app (cljfx/create-app
              (cljfx/wrap-add-map-event-handler
                #(when (and (= :event/scene-key-press (:event %))
                            (= KeyCode/ESCAPE (-> % :cljfx/event :code)))
                   (reset! *state [:stage
                                   {:style :transparent}]))))]
    (cljfx/mount-app *state app)))

#_(map-event-handler)

;; matter components
(defn matter-component []
  (let [*matter (atom (matter/make
                        {::showing (constantly true)
                         ::title (constantly "App title")}))
        content (fn [matter]
                  [:v-box
                   [:label "title"]
                   [:text-field
                    {:on-key-released {:event :event/change-text}}
                    (matter/sub matter ::title)]])
        root (fn [matter]
               [:stage
                {:showing (matter/sub matter ::showing)
                 :title (matter/sub matter ::title)}
                [:scene
                 [content]]])
        app (cljfx/create-app
              (comp
                (matter-cljfx/wrap-expose-matter)
                (cljfx/wrap-add-map-event-handler
                  #(swap! *matter matter/set-value ::title (-> %
                                                               :cljfx/event
                                                               :target
                                                               .getText)))
                (cljfx/wrap-map-value (constantly [root]))))]
    (cljfx/mount-app *matter app)))

#_(matter-component)

(defn stateful-example []
  (let [*state (atom {:first-name "Vlad"
                      :last-name "Protsenko"})
        text-input (fn [state label-text key]
                     [:v-box
                      [:label label-text]
                      [:text-field
                       {:on-text-changed #(swap! *state assoc key %)}
                       (get state key)]])
        root (fn [state]
               [:stage
                {:showing true}
                [:scene
                 [:v-box
                  [:label (str "You are " (:first-name state) " " (:last-name state) "!")]
                  [text-input "First Name" :first-name]
                  [text-input "Last Name" :last-name]]]])
        app (cljfx/create-app
              (comp
                (cljfx/wrap-expose-value)
                (cljfx/wrap-map-value (constantly [root]))))]
    (cljfx/mount-app *state app)))

#_(stateful-example)

(defn fn-fx-style-example []
  (let [*state (atom {:first-name "Vlad"
                      :last-name "Protsenko"})
        text-input (fn [label value key]
                     [:v-box
                      [:label label]
                      [:text-field
                       {:on-text-changed #(do (prn :on-text-changed %)
                                              (swap! *state assoc key %))
                        :text value}]])
        root (fn [{:keys [first-name last-name]}]
               [:stage
                {:showing true}
                [:scene
                 [:v-box
                  [:label (str "You are " first-name " " last-name "!")]
                  [text-input "First Name" first-name :first-name]
                  [text-input "Last Name" last-name :last-name]]]])
        app (cljfx/create-app
              (cljfx/wrap-map-value root))]
    (cljfx/mount-app *state app)))

#_(fn-fx-style-example)