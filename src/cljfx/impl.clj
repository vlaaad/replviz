(ns cljfx.impl)

(def ^:dynamic *opts*
  "Option map available for components during create/advance/delete fn execution"
  nil)