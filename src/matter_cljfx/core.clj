(ns matter-cljfx.core
  (:require [cljfx.lifecycle :as lifecycle]
            [cljfx.component :as component]
            [cljfx.middleware :as middleware]
            [matter.api :as matter]
            [cljfx.impl :as impl]))

(def exposed-matter-fn-lifecycle
  (with-meta {}
             {`lifecycle/create
              (fn [_ desc]
                (let [tag (first desc)
                      child-desc (apply matter/sub (::matter impl/*opts*) desc)]
                  (with-meta {:tag tag
                              :child (lifecycle/create-component child-desc)}
                             {`component/tag :tag
                              `component/instance #(component/instance (:child %))})))

              `lifecycle/advance
              (fn [_ component new-desc]
                (let [new-child-desc (apply matter/sub (::matter impl/*opts*) new-desc)]
                  (update component :child lifecycle/advance-component new-child-desc)))

              `lifecycle/delete
              (fn [_ component]
                (lifecycle/delete-component (:child component)))}))

(defn wrap-expose-matter []
  (fn [render-fn]
    (fn [component value opts]
      (render-fn component value (-> opts
                                     (assoc ::matter value)
                                     (update :cljfx.opt/tag->component-lifecycle
                                             middleware/add-tag->component-lifecycle-fn
                                             #(when (fn? %) exposed-matter-fn-lifecycle)))))))
