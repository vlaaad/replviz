(ns matter-test
  (:require [clojure.test :refer :all]
            [testit.core :refer :all]
            [matter.api :as matter]))

(defn- wrap-tracking [*tracker k f]
  (fn [& args]
    (let [ret (apply f args)]
      (swap! *tracker update k (fnil conj []) {:args (rest args)
                                               :ret ret})
      ret)))

(defn- track-sub-calls [*tracker m]
  (->> m
    (map (juxt key
               #(apply wrap-tracking *tracker %)))
    (into {})))

(deftest matter-allows-subscriptions-to-call-each-other
  (let [matter (matter/make {:db (constantly 1)
                             :inc-db #(inc (matter/sub % :db))})]
    (fact
      (matter/sub matter :inc-db) => 2)))

(deftest subscription-results-are-memoized
  (let [*tracker (atom {})
        matter (matter/make
                 (track-sub-calls *tracker
                                  {:fib (fn [matter n]
                                          (if (or (= 1 n) (zero? n))
                                            1
                                            (+ (matter/sub matter :fib (- n 2))
                                               (matter/sub matter :fib (dec n)))))}))]
    (matter/sub matter :fib 10)
    (fact
      (-> @*tracker :fib count) => 11)))

(let [*tracker (atom {})
      matter (matter/make
               (track-sub-calls *tracker
                                {:fib (fn [matter n]
                                        (if (or (= 1 n) (zero? n))
                                          1
                                          (+ (matter/sub matter :fib (- n 2))
                                             (matter/sub matter :fib (dec n)))))}))]
  (matter/sub matter :fib 2)
  @*tracker)

(deftest after-changing-matter-only-affected-subscriptions-are-recalculated
  (let [*tracker (atom {})
        matter-1 (matter/make
                   (track-sub-calls *tracker
                                    {:db (constantly {:user {:name "vlaaad"}})
                                     :username #(-> % (matter/sub :db) :user :name)
                                     :greeting #(str "Hello, " (matter/sub % :username) "!")}))]

    (facts
      (matter/sub matter-1 :greeting) => "Hello, vlaaad!"
      (-> @*tracker :username count) => 1
      (-> @*tracker :greeting count) => 1)
    (let [matter-2 (matter/advance-value matter-1 :db assoc-in [:user :age] 28)]
      (fact
        "Greeting is unchanged"
        (matter/sub matter-2 :greeting) => "Hello, vlaaad!")
      (fact
        "Since [:db] changed, [:user-name] is recalculated"
        (-> @*tracker :username count) => 2)
      (facts
        "Since after recalculating [:user-name] it stays the same, greeting is not recalculated"
        (-> @*tracker :greeting count) => 1))))

(deftest subscription-deps-are-updated-on-recalculation
  (let [*tracker (atom {})
        matter-1 (matter/make
                   (track-sub-calls *tracker
                                    {:db (constantly {:lang :en
                                                      :templates {:en "Hello, %s!"
                                                                  :ru "Привет, %s!"}})
                                     :lang #(:lang (matter/sub % :db))
                                     :en-template #(-> % (matter/sub :db) :templates :en)
                                     :ru-template #(-> % (matter/sub :db) :templates :ru)
                                     :template #(case (matter/sub % :lang)
                                                  :en (matter/sub % :en-template)
                                                  :ru (matter/sub % :ru-template))}))]
    (facts
      "Template depends on [:lang] and [:en-template] subscriptions"
      (matter/sub matter-1 :template) => "Hello, %s!"
      (-> @*tracker :template count) => 1)
    (let [matter-2 (matter/advance-value matter-1 :db assoc :lang :ru)]
      (facts
        "After changing [:lang] [:template] depends on [:lang] and [:ru-template] subscriptions"
        (matter/sub matter-2 :template) => "Привет, %s!"
        (-> @*tracker :template count) => 2)
      (let [matter-3 (matter/advance-value matter-2 :db assoc-in [:templates :en] "Hi, %s!")]
        (facts
          "Since [:template] no longer depends on [:en-template], it's not recalculated"
          (matter/sub matter-3 :template) => "Привет, %s!"
          (-> @*tracker :template count) => 2)))))

(deftest creating-derived-matters-inside-subs-add-dependency-on-matter-itself
  (let [*tracker (atom {})
        matter (matter/make
                 (track-sub-calls *tracker
                                  {:db (constantly 1)
                                   :sub-matter (fn [matter]
                                                 (let [str-db (str (matter/sub matter :db))]
                                                   (matter/advance-subs matter assoc :str-db (constantly str-db))))}))]
    (matter/sub matter :sub-matter)
    (let [matter-2 (matter/advance-subs matter assoc :other-db (constantly 2))]
      (matter/sub matter-2 :sub-matter))
    (fact
      "If update-subs is used inside sub fn, this sub starts to depend on whole matter"
      (-> @*tracker :sub-matter count) => 2)))

(deftest double-invalidations-don't-trigger-recalculations
  (let [*tracker (atom {})
        matter-1 (matter/make
                   (track-sub-calls *tracker
                                    {:db (constantly 1)
                                     :str-db #(str (matter/sub % :db))}))
        res-1 (matter/sub matter-1 :str-db)
        _ (matter/advance-subs matter-1 assoc :db (constantly 2))
        matter-3 (matter/advance-subs matter-1 assoc :db (constantly 3))
        res-3 (matter/sub matter-3 :str-db)]
    (facts
      "Values between multiple invalidations are recalculated"
      res-1 => "1"
      res-3 => "3")
    (fact
      "Recalculations happen on demand and not on invalidation"
      (-> @*tracker :str-db count) => 2)))

(deftest cross-matter-dependencies-are-not-tracked
  (let [*tracker (atom {})
        input-matter (matter/make {:db (constantly 1)})
        matter-a-1 (matter/make
                     (track-sub-calls *tracker
                                      {:db (constantly :a)
                                       :sub (fn [_ other-matter]
                                              (matter/sub other-matter :db))}))
        _ (matter/sub matter-a-1 :sub input-matter)
        matter-a-2 (matter/advance-subs matter-a-1 assoc :db (constantly :b))
        _ (matter/sub matter-a-2 :sub input-matter)]
    (fact
      "When calling sub to other matter inside subscription function, it does not begin to depend on it"
      (-> @*tracker :sub count) => 1)))

(deftest matters-are-repl-friendly
  (let [*leaked-matter (atom nil)
        *tracker (atom {})
        matter-1 (matter/make
                   (track-sub-calls *tracker
                                    {:db (constantly 1)
                                     :other-db (constantly :a)
                                     :leaking-sub (fn [matter]
                                                    (reset! *leaked-matter (matter/unbind matter))
                                                    (inc (matter/sub matter :db)))}))]
    (fact
      (matter/sub matter-1 :leaking-sub) => 2)
    (fact
      (matter/sub @*leaked-matter :other-db) => :a)
    (let [matter-2 (matter/advance-value matter-1 :other-db (constantly :b))]
      (fact
        (matter/sub matter-2 :leaking-sub) => 2)
      (fact
        "Even though we leaked sub's matter and subscribed to it later, it does not add dependency on leaked subscription"
        (-> @*tracker :leaking-sub count) => 1))))

(deftest you-can-subscribe-to-functions-directly
  (let [*tracker (atom {})
        matter (matter/make {:db (constantly 1)})
        inc-db (wrap-tracking *tracker :custom-fn #(inc (matter/sub % :db)))
        _ (facts
            "Subscribing to function directly calculates result"
            (matter/sub matter inc-db) => 2
            (-> @*tracker :custom-fn count) => 1)
        matter-2 (matter/advance-value matter :db inc)
        _ (facts
            "Updating context with dependent value triggers recalculation"
            (matter/sub matter-2 inc-db) => 3
            (-> @*tracker :custom-fn count) => 2)
        matter-3 (matter/advance-subs matter-2 assoc :other-db (constantly :a))
        _ (facts
            "Updating context with independent change does not trigger recalculation"
            (matter/sub matter-3 inc-db) => 3
            (-> @*tracker :custom-fn count) => 2)]))
